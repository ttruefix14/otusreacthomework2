
import { PayloadAction, createSlice } from "@reduxjs/toolkit";
import { User } from "../models/user";
import { Statement } from "typescript";

type action = "signup" | "signin";

interface State {
    users: User[];
    currentUser?: User;
    signedUp: boolean;
    signedIn: boolean;
}

const initialState: State = {
    users: [],
    signedUp: false,
    signedIn: false,
};

export const userSlice = createSlice({
    name: 'user',
    initialState,
    reducers: {
        signUp: (state: State, action: PayloadAction<User>) => {
            const users = [...state.users];
            const user = action.payload;
            const index = users.findIndex(x => x.username === user.username);
            if (index === -1) {
                users.push(user);
                state.signedUp = true;
            } else {
                alert(`Пользователь "${user.username}" уже зарегистрирован, повторите попытку`);
                state.signedUp = false;
            }
            state.users = users;
        },
        login: (state: State, action: PayloadAction<User>) => {
            const users = [...state.users];
            const user = action.payload;
            const index = users.findIndex(x => x.username === user.username);
            if (index === -1) {
                alert(`Пользователь "${user.username}" не зарегистрирован, повторите попытку`);
                state.signedIn = false;
            } else {
                if (user.password === users[index].password) {
                    state.currentUser = user;
                    state.signedIn = true;
                } else {
                    alert("Неверный пароль! Повторите попытку")
                }
            }
        },
        cancelAction: (state: State, action: PayloadAction<action>) => {
            switch (action.payload) {
                case "signup":
                    state.signedUp = false;
                    break;
                case "signin":
                    state.signedIn = false;
                    break;
                default:
                    break;
            }
        },
        clearUser: (state: State) => {
            state.currentUser = undefined;
        },
    },
});

export const { signUp, login, cancelAction, clearUser } = userSlice.actions;

export default userSlice.reducer;