import { configureStore } from "@reduxjs/toolkit";
import userReducer from "./stateManagement/user-reducer";

const store = configureStore({
    reducer: {
        user: userReducer,
    },
});

export default store;