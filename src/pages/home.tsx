import { connect, useDispatch } from "react-redux";
import { User } from "../models/user";
import { Button } from "react-bootstrap";
import { clearUser } from "../stateManagement/user-reducer";

interface Props {
    currentUser?: User;
}

const Home = (props: Props) => {
    const dispatch = useDispatch();

    const { currentUser } = props;

    const quit = () => {
        dispatch(clearUser());
    };

    return <>
        <h1>Домашняя страничка</h1>
        <p>Домашняя страница домашнего задания по React № 2</p>
        {currentUser !== undefined ?
            <><p>Пользователь "{currentUser.username}" с паролем "{currentUser.password}" авторизован! </p>
                <Button variant="primary" onClick={quit}>Выйти</Button></>
            : <p>Вы не авторизованы! Пожалуйста, залогиньтесь или зарегистрируйтесь!</p>}
    </>;
}

const mapReduxStateToProps = (gs: any): Props => {
    return {
        currentUser: gs.user.currentUser,
    }
}

export default connect(mapReduxStateToProps)(Home);