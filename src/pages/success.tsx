interface Props {
    text: string;
}

const Success = (props: Props) => {
    return <div>
        <p>{props.text} прошла успешно!</p>
    </div>
}

export default Success;