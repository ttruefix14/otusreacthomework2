import { connect, useDispatch } from "react-redux";
import { User } from "../models/user";
import { cancelAction } from "../stateManagement/user-reducer";
import { useNavigate } from "react-router-dom";

interface Props {
    currentUser?: User;
    signedIn: boolean;
}
    
const SignedIn = () => {
    return <div>
        <p>Вы уже авторизованы, разлогиньтесь</p>
    </div>;
};

export const withSignedInScreen = <P extends object>(Com: React.ComponentType<P>) =>
    (props: P & Props) => {
        const dispatch = useDispatch();
        const navigate = useNavigate();

        if (props.signedIn) {
            dispatch(cancelAction("signin"));
            navigate("success");
        }
        return props.currentUser !== undefined ? <SignedIn></SignedIn> : <Com {...props}></Com>
    };

