import { Button, Form } from "react-bootstrap";
import "./login.scss";
import { connect, useDispatch, useSelector } from "react-redux";
import { useState } from "react";
import { login } from "../../stateManagement/user-reducer";
import { useNavigate } from "react-router-dom";
import { User } from "../../models/user";

const Login = () => {
    const [username, setUsername] = useState<string>();
    const [password, setPassword] = useState<string>();

    const dispatch = useDispatch();

    const users = useSelector((gs: any) => gs.user.users);

    const onUsernameChange = (e: any) => {
        setUsername(e.target.value);
    };

    const onPasswordChange = (e: any) => {
        setPassword(e.target.value);
    };

    const onFormSubmit = (e: any) => {
        e.preventDefault();
        dispatch(login({ username: username ?? "", password: password ?? "" }));
    };

    return <>
        <h1>Страница входа</h1>
        <div className="login">
            <Form onSubmit={onFormSubmit}>
                <Form.Group className="mb-3" controlId="formBasicName">
                    <Form.Label>Имя пользователя</Form.Label>
                    <Form.Control type="text" placeholder="Введите имя пользователя" onChange={onUsernameChange} />
                    <Form.Text className="text-muted">

                    </Form.Text>
                </Form.Group>

                <Form.Group className="mb-3" controlId="formBasicPassword" onChange={onPasswordChange} >
                    <Form.Label>Пароль</Form.Label>
                    <Form.Control type="password" placeholder="Введите пароль" />
                </Form.Group>
                <Button variant="primary" type="submit">
                    Отправить
                </Button>
            </Form>
        </div>
    </>
}

export default Login;