import { Button, Form } from "react-bootstrap";
import "./sign-up.scss";
import { useState } from "react";
import { connect, useDispatch, useSelector } from "react-redux";
import { cancelAction, signUp } from "../../stateManagement/user-reducer";
import { User } from "../../models/user";
import { useNavigate } from "react-router-dom";

interface Props {
    signedUp: boolean;
}

const SignUp = (props: Props) => {
    const [username, setUsername] = useState<string>();
    const [password, setPassword] = useState<string>();

    const dispatch = useDispatch();

    const navigate = useNavigate();

    if (props.signedUp) {
        dispatch(cancelAction("signup"));
        navigate("success")
    };

    const onUsernameChange = (e: any) => {
        setUsername(e.target.value);
    };

    const onPasswordChange = (e: any) => {
        setPassword(e.target.value);
    };

    const onFormSubmit = (e: any) => {
        e.preventDefault();
        dispatch(signUp({ username: username ?? "", password: password ?? "" }));
    };

    return <>
        <h1>Страница регистрации</h1>
        <div className="sign-up">
            <Form onSubmit={onFormSubmit}>
                <Form.Group className="mb-3" controlId="formBasicName">
                    <Form.Label>Имя пользователя</Form.Label>
                    <Form.Control type="text" placeholder="Введите имя пользователя" onChange={onUsernameChange} />
                    <Form.Text className="text-muted">

                    </Form.Text>
                </Form.Group>

                <Form.Group className="mb-3" controlId="formBasicPassword">
                    <Form.Label>Пароль</Form.Label>
                    <Form.Control type="password" placeholder="Введите пароль" onChange={onPasswordChange} />
                </Form.Group>
                <Button variant="primary" type="submit">
                    Отправить
                </Button>
            </Form>
        </div>
    </>
}

const mapReduxStateToProps = (gs: any): Props => {
    return {
        signedUp: gs.user.signedUp,
    }
};

export default connect(mapReduxStateToProps)(SignUp);