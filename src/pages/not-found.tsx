import grumpy from "../assets/not-found-img.png";
import "./not-found.scss";

const NotFound = () => {
    return <div className="not-found">
        <img src={grumpy} alt=""></img>
        <div>
            <h1>404 Page not found</h1>
            <p>Такой страницы не существует на моём сайте</p>
        </div>
    </div>
}

export default NotFound;