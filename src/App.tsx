import React from 'react';
import './App.scss';
import { BrowserRouter, Link, Route, Routes } from 'react-router-dom';
import { Col, Container, Nav, Navbar, Row } from 'react-bootstrap';
import NotFound from './pages/not-found';
import Home from './pages/home';
import Login from './pages/login/login';
import SignUp from './pages/sign-up/sign-up';
import Success from './pages/success';
import { withSignedInScreen } from './pages/signed-in';
import { User } from './models/user';
import { connect } from 'react-redux';

const LoginWithSignedInScreen = withSignedInScreen(Login);
const SignUpWithSignedInScreen = withSignedInScreen(SignUp);

interface Props {
  currentUser?: User;
  signedIn: boolean;
}

function App(props: Props) {
  return (
    <BrowserRouter>
      <Navbar expand="lg" className="bg-body-tertiary">
        <Container>
          <Navbar.Brand href="/">OTUS React Homework 2</Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="me-auto">
              <Nav.Link as={Link} to="/">Home</Nav.Link>
              <Nav.Link as={Link} to="/login">Login</Nav.Link>
              <Nav.Link as={Link} to="/signup">Sign Up</Nav.Link>
              <Nav.Link as={Link} to="/somepage">Some Page</Nav.Link>
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
      <Container className="container-centered">
        <Routes>
          <Route path="/" element={<Home></Home>}></Route>
          <Route path="login" element={<LoginWithSignedInScreen currentUser={props.currentUser} signedIn={props.signedIn}></LoginWithSignedInScreen>}></Route>
          <Route path="login/success" element={<Success text="Авторизация"></Success>}></Route>
          <Route path="signup" element={<SignUpWithSignedInScreen currentUser={props.currentUser} signedIn={props.signedIn}></SignUpWithSignedInScreen>}></Route>
          <Route path="signup/success" element={<Success text="Регистрация"></Success>}></Route>
          <Route path="*" element={<NotFound></NotFound>}></Route>
        </Routes>
      </Container>
    </BrowserRouter>

  );
}

const mapReduxStateToProps = (gs: any): Props => {
  return {
      currentUser: gs.user.currentUser,
      signedIn: gs.user.signedIn,
  }
};

export default connect(mapReduxStateToProps)(App);
